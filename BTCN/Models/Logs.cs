﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace BTCN.Models
{
    public class Logs
    {
        [Key]
        public int logId { get; set; }
        public string LoginDate { get; set; }
        public string LoginTime { get; set; }
        [JsonIgnore]
        public List<Transactions>? transactions { get; set; }
        [JsonIgnore]
        public Reports? report { get; set; }
    }
}


