﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace BTCN.Models
{
    public class Accounts
    {
        [Key]
        public int AccountId { get; set; }
        public string Name { get; set; }
        [JsonIgnore]
        public List<Customer>? Customers { get; set; }
        [JsonIgnore]
        public Reports? report { get; set; }
    }
}

