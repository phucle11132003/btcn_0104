﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace BTCN.Models
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext>
        options) : base(options)
        { }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Accounts> Accounts { get; set; }
        public DbSet<Employees> Employees { get; set; }
        public DbSet<Logs> Logs { get; set; }
        public DbSet<Reports> Reports { get; set; }
        public DbSet<Transactions> Transactions { get; set; }
    }
}
