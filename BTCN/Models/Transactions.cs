﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace BTCN.Models
{
    public class Transactions
    {
        [Key]
        public int TransactionId { get; set; }
        public string Name { get; set; }
        [JsonIgnore]
        public List<Customer>? Customers { get; set; }
        [JsonIgnore]
        public List<Employees>? Employees { get; set; }
        [JsonIgnore]
        public Logs? Logs { get; set; }
        [JsonIgnore]
        public Reports? Report { get; set; }
    }
}

